<?php
require_once './../lib/dbHandler.php';

class tests extends PHPUnit_Framework_TestCase
{

    public function testConnection(){

        $handlerDB = new HandlerDB();
        $status = $handlerDB->connectDB();

        $this->assertNotNull($status);
    }

    public function testSetPerson() {
        var_dump("Test set person");

        $data = array();
        $data["dni"]= "72727875R";
        $data["nombre"]= "Rafa";
        $data["apellido"] = "Nadal";
        $data["edad"] = "30";

        $handlerDB = new HandlerDB();
        $response = $handlerDB->insertData("personas", $data);
        var_dump($response);

        $this->assertNotFalse($response);
        //$this->assertFalse($response);
    }
    public function testSetPersonWrong() {
        var_dump("Test set person wrong");

        $data = array();
        $data["dnis"]= "72727875R";
        $data["algo"]= "";
        $data["mal"] = "Nadal";
        $data["edad"] = 30;

        $handlerDB = new HandlerDB();
        $response = $handlerDB->insertData("personas", $data);

        var_dump($response);

        $this->assertFalse($response);
    }

    public function testGetPeople() {

        $handlerDB = new HandlerDB();
        $query = "SELECT * FROM personas";
        $actual = $handlerDB->getAllDataFrom("personas", $query);
        var_dump("test get people:");

        var_dump($actual);
        file_put_contents("../log_files/log.txt",json_encode($actual), FILE_APPEND);

        $this->assertArrayHasKey('0',$actual);
    }

    public function testDeletePerson(){

        var_dump("test delete person");

        $handlerDB = new HandlerDB();
        $response = $handlerDB->deleteData("personas", "dni","72727875R");

        var_dump($response);
        $this->assertNotFalse($response);

    }
}