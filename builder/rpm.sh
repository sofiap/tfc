#!/bin/sh

set -e
echo "###GENERATE ZIP Package###"
dir=`dirname $0`
if [ -n "$dir" ]; then
  dir="$dir/"
fi

BUILD_NUMBER="${BUILD_NUMBER}"
echo "Doing Build for version " ${BUILD_NUMBER} "and branch master"

initplace=$PWD

echo "###Current initplace path###"
echo $initplace

echo "============================================================================="
echo "=========================== Run PhpUnit Tests ================================"
phpunit tests ../tests/tests.php
echo "============================================================================="


cd $initplace/../
echo "LISTING"
ls -latr
echo "LIST DONE"

name='TFC-'${BUILD_NUMBER}
zip -r ./../builds/$name ./

exit $rc