<?php
header("access-control-allow-origin: *");
require 'vendor/autoload.php';
require_once '/lib/dbHandler.php';

$app = new \Slim\Slim();
$app->get('/getPeople', 'getPeople');
$app->get('/setPerson', 'setPerson');
$app->get('/deletePerson', 'deletePerson');
$app->run();

/**
 * return the people stored
 */
function getPeople(){

    try {

        $app = \Slim\Slim::getInstance();

        $handlerDB = new HandlerDB();
        $query = "SELECT * FROM personas";
        $people = $handlerDB->getAllDataFrom("personas", $query);

        $app->response->header('Content-Type', 'application/json');
        $app->response->body(json_encode($people));

    } catch (\Exception $e) {

        $app->response->setStatus(400);
        $app->response->body('Bad Request: ' . $e->getMessage());
    }
}

/**
 * Saves new people booking
 */
function setPerson (){

    try {

        $app = \Slim\Slim::getInstance();

        $app->response->header('Content-Type', 'text/html');
        if (!empty($_GET)) {

            $data = array();
            $data["dni"]= $app->request->get('dni');
            $data["nombre"]= $app->request->get('nombre');
            $data["apellido"] = $app->request->get('apellido');
            $data["edad"] = $app->request->get('edad');

            $handlerDB = new HandlerDB();
            $response = $handlerDB->insertData("personas", $data);
            $app->response->setBody($response);
        }else{
            throw new Exception("Not data specified to add new registry");
        }
    } catch (\Exception $e) {
        $app->response->setStatus(400);
        $app->response->body('Bad Request: ' . $e->getMessage());
    }
}


/* Delete Person by DNI
*/
function deletePerson(){
    try {

        $app = \Slim\Slim::getInstance();

        $app->response->header('Content-Type', 'text/html');
        if (!empty($_GET)) {

            $dni= $app->request->get('dni');

            $handlerDB = new HandlerDB();
            $response = $handlerDB->deleteData("personas",'dni', $dni);
            $app->response->setBody($response);
        }else{
            throw new Exception("Not data specified to add new registry");
        }
    } catch (\Exception $e) {
        $app->response->setStatus(400);
        $app->response->body('Bad Request: ' . $e->getMessage());
    }

}