<?php

// This class helps you to handle your queries to Mysql Database
class HandlerDB{
    private $dbLink;
    private $controlledErrors;
    
    /**
     * Database host
     * @var string
     */
    private $host = "localhost";
    /**
     * Database user
     * @var string
     */
    private $userDB = "testDB";

    /**
     * Database password
     * @var string
     */
    private $password = "123qwe";

    /**
     * Database name
     * @var string
     */
    private $dataBase = "proyecto";

    /**
     * HandlerDB constructor.
     */
    public function __construct(){

        $this->dbLink = $this->connectDB();
        $this->controlledErrors = array(1022,1054,1062);
    }

    public function __destruct()
    {
        $this->dbLink = null;
    }

    /**
     * @return \PDO connection to data base
     */
    public function connectDB(){
        try {
            $db = new \PDO("mysql:host=".$this->host.";dbname=".$this->dataBase."", $this->userDB, $this->password);
            $db->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
            return($db);
        }catch(\PDOException $e){
            return null;
        }
    }


    /**
     * @param $query: it is contain the objetc tha you use to send the query to the server
     * @return string: an error message
     */
    private function checkQueryExecution($query){
        $error = $this->dbLink->errorInfo();
        if(in_array($error[1], $this->controlledErrors)){
            return false;
        }else{
            return $query;
        }
    }

    /**
     * It execute the queries on DataBase
     * @param string $sql with your query
     * @return string the result of SQL query or false
     */
    private function execQuery($sql){
        $result=$this->dbLink->query($sql);
        $exc = $this->checkQueryExecution($result);
        return $exc;
    }

    /**
     * It execute querys from a file. This querys have to be written correctly and one each line.
     * @param $sqlFilePath : the path of you sql script
     * @param $connection: you pass this param if you want to use a specific connection. Example: you want to create a Database
     */
    public function execDBQueriesFromFile($sqlFilePath,$connection= null){

        if($this->dbLink){
            if($connection==null){
                $connection=$this->dbLink;
            }
            $file = fopen($sqlFilePath, "r") or exit("Unable to open file!");
            while(!feof($file)) {
                $sql=fgets($file);
                $result=$connection->query($sql);
            }
            fclose($file);
        }
    }

    /**
     * Insert data on a specific table
     * @param $tableName: is the name of the table
     * @param $fields: is an associative array with the fields and the new values that you want to insert
     */
    public function insertData($tableName,$fields){
        if($this->dbLink){
            $fieldsToIns = "";
            $valuesToIns = "";
            foreach($fields as $key=>$value){
                $fieldsToIns.= $key.",";
                //It checks if the value is a string because when you do the query you need that the strings are between ""
                if(gettype($value)=="string"){
                    $valuesToIns.= "\"$value\"".",";
                }else{
                    $valuesToIns.= $value.",";
                }
            }

            $fieldsToIns = substr($fieldsToIns, 0,(strlen($fieldsToIns)-1) );
            $valuesToIns = substr($valuesToIns, 0,(strlen($valuesToIns)-1) );

            $sql = "insert into ".$tableName." ($fieldsToIns) values ($valuesToIns)";
            return $this->execQuery($sql);
        }
    }


    /**
     * It gets all data from data base or data of a specific client or clients
     * @param $tableName: name of table in DB
     * @param $sql: you can pass this parameter if you want to do a specific consult to database
     * @return array if the consult has data or false if it does not it
     */
    public function getAllDataFrom($tableName, $sql=null){
        if($this->dbLink){
            $result=null;
            if($sql!=null){
                if (preg_match("/\bselect\b/i", $sql) && preg_match("/\bfrom\b/i", $sql)) {
                    $result=$this->execQuery($sql);
                }
            }else{
                $sql="select * from ".$tableName;
                $result= $this->execQuery($sql);
            }

            if($result){
                $lines= array();
                while ($line = $result->fetch(\PDO::FETCH_ASSOC)) {
                    $lines[]= $line;
                }
                return $lines;
            }
        }
        return false;
    }

    /**
     * @param $sql: the sentence that you want to execute
     */
    public function updateData($sql){

        if($this->dbLink){
            if (preg_match("/\bupdate\b/i", $sql) && preg_match("/\bset\b/i", $sql)) {
                $this->execQuery($sql);
            }
        }
    }

    /**
     * @param $tableName: name of table
     * @param $primaryKey: id field that ensure that you delete the correct registry
     * @param $valuePK : value of previous parameter
     */
    public function deleteData($tableName,$primaryKey,$valuePK){
        if ($this->dbLink) {
            if (gettype($valuePK) == "string") {
                $sql = "delete from " . $tableName . " where " . $primaryKey . "='" . $valuePK . "'";
            } else {
                $sql = "delete from " . $tableName . " where " . $primaryKey . "=" . $valuePK;
            }
            return $this->execQuery($sql);
        }
    }
}